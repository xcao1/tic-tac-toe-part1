// ========================================
// CS570 Xinlei Cao
// Tic-Tac-Toe
// smallest built version
// ========================================

// head files
#include <iostream>

// using name space
using std::cin;
using std::cout;
using std::endl;


// ========================================
//used to memerise the chess board , update the chess board with this array , row|column
int chessBoard[3][3];

// check if the chess board is in a correct state
bool checkChessBoradStatus()
{
	bool status = true;// true when valid

	int player1 = 0;// count for player1's actions
	int player2 = 0;
	for( int i = 0; i != 3; i ++ )
	{
		for( int j = 0; j != 3; j ++ )
		{
			if( chessBoard[i][j] < -1 || chessBoard[i][j] > 1 )
				status = false;
			if( chessBoard[i][j] == 1 )
				player1 ++;
			if( chessBoard[i][j] == -1 )
				player2 ++;
		}
	}
	if( player1 < 0 || player1 > 5 )
		status = false;
	if( player2 < 0 || player2 > 5 )
		status = false;

	return status;
}

// reset the chess board to innitial state
void resetChessBoard()
{
	for( int i = 0; i != 3; i ++ )
	{
		for( int j = 0; j != 3; j ++ )
		{
			chessBoard[i][j] = 0;
		}
	}
}

// ==================================================
// action , each action contains a row number and a column number
int row;
int column;

// check if the action is legal
bool isActionLegal( int inputRow, int inputColumn )
{	
	// if the row number is correct
	if( inputRow > 3 || inputRow < 1 )
	{
		std::cerr<<"error : row number is incorrect.";
		return false;
	}
	else if( inputColumn > 3 || inputColumn < 1 )
	{
		std::cerr<<"error : column number is incorrect.";
		return false;
	}
	else
	{
		// if there is already a piece at that position
		if( chessBoard[ inputRow - 1 ][ inputColumn - 1 ] == 0 )
			return true;
		else
		{
			std::cerr<<"there is already a piece at the position.";
			return false;
		}
	}
}

// update the chess board
// whosAction : Player1 = 1, Player2 = -1, None = 0
void updateChessBoard( int inputRow, int inputColumn , int whosAction )
{
	// check if the chess board is already in wrong status
	if( checkChessBoradStatus == false )
	{
		// error
		std::cerr<<"the chess board is not in a correct status, it will not be updated.";
	}
	else
	{
		if( whosAction != -1 && whosAction != 0 && whosAction != 1 )
		{
			// error
			std::cerr<<"the action value can only be -1,0,1.";
		}
		else
		{
			if( isActionLegal( inputRow, inputColumn ) )
			{
				// update the chess board
				chessBoard[ inputRow - 1 ][ inputColumn - 1 ] = whosAction;
			}
			else
			{
				// illegal action
				std::cerr<<"illegal action.";
			}
		}
	}
}

// print the chess board in a acceptable way to the screen
void printChessBoard()
{
	// convert the original chess board array into characters
	char printedChessBoard[3][3];
	for( int i = 0; i != 3; i ++ )
	{
		for( int j = 0; j != 3; j ++ )
		{
			switch ( chessBoard[i][j] )
			{
			case 1:
				printedChessBoard[i][j] = 'X';
				break;
			case -1:
				printedChessBoard[i][j] = 'O';
				break;
			case 0:
				printedChessBoard[i][j] = ' ';
				break;
			default:
				// error
				std::cerr<<"error : the chess board array can only have -1,0,1 as its value.";
				return;
				break;
			}
		}
	}
	// print the chess board
	cout<<"\n\nChess Borad :"<<endl;
	cout<<"  1    2    3  "<<endl;
	cout<<"1 "<<printedChessBoard[0][0]<<" |  "<<printedChessBoard[0][1]<<" |  "<<printedChessBoard[0][2]<<endl;
	cout<<" ---+----+-----"<<endl;
	cout<<"2 "<<printedChessBoard[1][0]<<" |  "<<printedChessBoard[1][1]<<" |  "<<printedChessBoard[1][2]<<endl;
	cout<<" ---+----+-----"<<endl;
	cout<<"3 "<<printedChessBoard[2][0]<<" |  "<<printedChessBoard[2][1]<<" |  "<<printedChessBoard[2][2]<<endl;
	cout<<"\n\n"<<endl;
}

// =============================
// player1 or player2
bool whichPlayersTurn;
// if game is end
bool gameOver;
// =============================
// who win
int whichPlayerWin;


// get input from the keyboard , interaction
void keyboard()
{
	// show it is which player's turn
	if( whichPlayersTurn )
	{
		cout<<"Player1's Turn."<<endl;
	}
	else
	{
		cout<<"Player2's Turn."<<endl;
	}
	// get input from keyboard
	do
	{
		cout<<"please input your action....."<<endl;
		cout<<"row : ";
		cin>>row;
		cout<<"colum : ";
		cin>>column;

	} while ( isActionLegal( row, column ) == false );
	
	
}
bool isEndLegal()
{
	if( whichPlayerWin != 1 && whichPlayerWin != -1 && whichPlayerWin != 0 )
	{
		// illegal
		std::cerr<<"( error: result can only be player1 win ,player2 win or a draw . )";
		return false;
	}
	else 
		return true;
}
// judge is one side has win the game
bool isGameOver()
{
	int sum;
	// check columns
	for( int i = 0; i != 3; i ++ )
	{
		sum = 0;
		for( int j = 0; j != 3; j ++ )
		{
			sum = sum + chessBoard[i][j];
		}
		if( sum == 3 || sum == -3 )
		{
			// end
			gameOver = true;
			if( sum == 3 )
				whichPlayerWin = 1;
			else
				whichPlayerWin = -1;
			return gameOver;
		}
	}
	// check rows
	for( int j = 0; j != 3; j ++ )
	{
		sum = 0;
		for( int i = 0; i != 3; i ++ )
		{
			sum = sum + chessBoard[i][j];
		}
		if( sum == 3 || sum == -3 )
		{
			// end
			gameOver = true;
			if( sum == 3 )
				whichPlayerWin = 1;
			else
				whichPlayerWin = -1;
			return gameOver;
		}
	}
	// check diagonal
	int sum1 = 0;
	int sum2 = 0;
	sum1 = chessBoard[0][0] + chessBoard[1][1] + chessBoard[2][2];
	sum2 = chessBoard[2][0] + chessBoard[1][1] + chessBoard[0][2];
	if( sum1 == 3 || sum2 == 3 )
	{
		gameOver = true;
		whichPlayerWin = 1;
	}
	if( sum1 == -3 || sum2 == -3 )
	{
		gameOver = true;
		whichPlayerWin = -1;
	}

	bool draw = true;
	for( int i = 0; i != 3; i ++ )
	{
		for( int j = 0; j != 3; j ++ )
		{
			if( chessBoard[i][j] == 0 )
			{
				draw = false;
			}
		}
	}
	if( draw == true )
	{
		gameOver = true;
		whichPlayerWin = 0;
	}
	return gameOver;
}
// update the input to the chess board , and judge if the game is end
void updateGame()
{
	// update the chess board
	if( whichPlayersTurn )
		updateChessBoard( row, column, 1 );
	else
		updateChessBoard( row, column, -1 );
	// check if the game is over
	isGameOver();
	// if not, continue the game
}

// manage which player's turn
void changePlayer()
{
	if( whichPlayersTurn )
		whichPlayersTurn = false;
	else
		whichPlayersTurn = true;
}

// print the winner
void printTheWinner()
{
	if( isEndLegal() )
	{
		cout<<"\nGame Over"<<endl;
		if( whichPlayerWin == 1 )
			cout<<"Player 1  win !! ";
		else if( whichPlayerWin == -1 )
			cout<<"Player 2  win !! ";
		else if( whichPlayerWin == 0 )
			cout<<"end in a draw !! ";
		else
			std::cerr<<"error end";
	}
}
// game loop
void runTheGame()
{
	// the innitial game loop state
	// not end
	gameOver = false;
	// player1's turn
	whichPlayersTurn = true;
	// if game over,quit from the loop
	while( gameOver != true )
	{
		// print the chess board
		printChessBoard();
		// get input from keyboard
		keyboard();
		// update the game
		updateGame();
		// change player
		changePlayer();
	}
	// print the final chess board
	printChessBoard();
	// print the result
	printTheWinner();
}


// main function
int main()
{
	// print the title
	cout<<"CS570 Xinlei Cao"<<endl;
	cout<<"============================================="<<endl;
	cout<<"====        Tic - Tac - Toe Game         ===="<<endl;
	cout<<"============================================="<<endl;
	
	// run the game loop
	runTheGame();

	// pause when debugging
	//std::system( "pause" );
	// end - of - main function
	return 0;
}